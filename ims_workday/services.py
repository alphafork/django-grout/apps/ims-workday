import datetime

from django.conf import settings as project_settings

from .models import Holiday, Workday


# Weekdays can be added in settings as an array with values -
# "Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"
def get_default_weekly_holidays():
    weekly_holidays = getattr(project_settings, "WEEKLY_HOLIDAYS", ["Sun"])
    weekdays = ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"]
    weekly_holidays_int = []
    for holiday in weekly_holidays:
        weekly_holidays_int.append(weekdays.index(holiday))
    return weekly_holidays_int


# workday will have prioriy over holiday
# date: <datetime.date>
def is_workday(date):
    holidays = Holiday.objects.all().values_list("date", flat=True)
    workday = Workday.objects.all().values_list("date", flat=True)
    if date in workday or (
        date.weekday() not in get_default_weekly_holidays() and date not in holidays
    ):
        return True
    return False


# positons: <"preceding"/"succeeding">, date: <datetime.date>
def get_adjacent_workday(position, date):
    if is_workday(date):
        return date

    if position == "preceding":
        date -= datetime.timedelta(1)
    elif position == "succeeding":
        date += datetime.timedelta(1)
    else:
        raise ValueError("accepted values are `preceding` and `succeeding`")
    return get_adjacent_workday(position, date)


def count_workdays(start_date, end_date, include_input_dates=True):
    if not include_input_dates:
        start_date += datetime.timedelta(1)
        end_date -= datetime.timedelta(1)
    day_count = 0
    while start_date <= end_date:
        if is_workday(start_date):
            day_count += 1
        start_date += datetime.timedelta(1)
    return day_count


def add_workdays_to_date(date, workdays):
    while workdays > 0:
        date += datetime.timedelta(1)
        if is_workday(date):
            workdays -= 1
    return date

from django.db import models
from ims_base.models import AbstractLog


class Holiday(AbstractLog):
    name = models.CharField(max_length=100)
    description = models.TextField(null=True, blank=True)
    date = models.DateField()


class Workday(AbstractLog):
    name = models.CharField(max_length=100)
    description = models.TextField(null=True, blank=True)
    date = models.DateField()
